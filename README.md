# DataTables Plugin For Grails 3
![table01.png](https://bitbucket.org/repo/8qzLeo/images/4214385229-table01.png)

This is a Grails plugin that allows you to quickly add feature-rich tables to your Grails application. It uses the excellent [DataTables plugin for jQuery](http://www.datatables.net/) created by SpryMedia Ltd.

This plugin provides the following components to your application:

* A taglib that you use to define the table in your GSP. The taglib generates all the HTML and JavaScript necessary to create the table.
* A controller and services that provide data for the table.
* An extensible reporting system.

To get started, view the project wiki at [https://bitbucket.org/ben-wilson/grails-datatables/wiki/Home](https://bitbucket.org/ben-wilson/grails-datatables/wiki/Home). Note that the wiki resides within the Grails 2 version of this plugin.